#include <iostream>
#include <time.h>


int DayWeek() {
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	return buf.tm_mday;
}

int main() {
	const int ArraySize{ 4 };
	int NumberString{ DayWeek() % ArraySize };
	int sum{ 0 };
	int Array[ArraySize][ArraySize]; // = { {0, 1, 2, 3}, {1, 2, 3, 4}, {2, 3, 4, 5}, {3, 4, 5, 6} };

	for (int i{ 0 }; i < ArraySize; i++) {

		for (int j{ 0 }; j < ArraySize; j++) {
			Array[i][j] = i + j;
			std::cout << Array[i][j];
		}
		std::cout << '\n';
	}

	for (int i{ 0 }; i < ArraySize; i++) {
		sum += Array[NumberString][i];
	}
	std::cout << std::endl << "Day of the month: " << DayWeek() << '\n';
	std::cout << "Line amount:" << NumberString << " equal to " << sum;
}